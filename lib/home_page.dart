import 'package:flutter/material.dart';
import 'package:product_list_order/product_page.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
          appBar: AppBar(
          title: Text("Product List"),
      centerTitle: true,
      ),
    body: Column(
      children: [
        Row(
          
          children: [
            Padding(padding: EdgeInsets.all(5)),

            InkWell(onTap: (){
              Navigator.push(context, MaterialPageRoute(builder: (context)=>ProductPage()));
            },
              child: Container(
                color: Colors.deepPurple,
                height: 180,
                width:170 ,
              ),
            ),
            SizedBox(width: 5,),
            Container(
              color: Colors.deepPurple,
              height: 180,
              width:170 ,
            ),
          ],
        )
      ],
    )));


}
}